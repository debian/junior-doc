# !/bin/sh
# copyright-
#   Copyright (C) 2002 by Ben Armstrong <synrg@debian.org>
# license-
#   This is software is licensed under the GNU Public License.
# purpose-
#   Create a list of Debian Jr. packages, and their install status.
# usage-
#   sh list-junior.sh
# output-
#   ./list-junior.html
# notes-
#   - This is an example script only.
#   - list-junior depends on: grep-dctrl, wdiff
#     - these are not declared as dependencies of junior-doc
#       because we wish to keep junior-doc architecture-independant
#     - TODO: look at making this a proper script with switches,
#       arguments, etc.
set -e

output_html=list-junior.html

# No need to let people muck with our temporaries
umask 077
dctrl_status=`mktemp /tmp/junior-doc-status.XXXXXX`
dctrl_available=`mktemp /tmp/junior-doc-available.XXXXXX`
dctrl_merged=`mktemp /tmp/junior-doc-merged.XXXXXX`
umask 002

grep-status junior- >$dctrl_status;
grep-available junior- >$dctrl_available;

# Unspeakably ugly.  see http://bugs.debian.org/136097
wdiff $dctrl_status $dctrl_available \
  |sed -e's/\[-//; s/-\]//; s/{+//; s/+}//' \
  |grep-dctrl -sPackage,Version,Status,Description 'junior-' >$dctrl_merged

# Make a pretty web page.
awk ' \
BEGIN { \
  print "<html>\n<head>\n<title>Debian Jr. Packages</title>\n<head>\n" \
    "<body>\n<u1>Debian Jr. Packages</u1><p>\n" \
    "<table>\n<tr><td>Package<td>Version<td>Status<td>Description" \
} \
/^Package/ {print "<tr valign=top><td><a href=\"http://packages.debian.org/" \
  $2 "\">" $2 "</a>"; next} \
/^(Status)/ {print "<td>" $4; next} \
/^(Version|Description)/ {$1=""; print "<td>" $0; next} \
END {print "</table></p></body></html>"} \
' $dctrl_merged >$output_html

# Be good and clean up our mess.
rm $dctrl_available $dctrl_status $dctrl_merged
